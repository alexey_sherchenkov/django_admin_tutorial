from django.db import models
from django.utils.html import mark_safe

# Create your models here.


class Product(models.Model):
    sku = models.CharField(max_length=100, verbose_name="СКУ")
    bar_code = models.IntegerField(verbose_name="Штрих код")
    description = models.TextField(max_length=600, verbose_name="Описание аромата")
    price = models.DecimalField(decimal_places=2, max_digits=8, verbose_name="Цена")
    url = models.URLField(verbose_name="Ссылка")
    categories = models.ManyToManyField(
        "Category", through="CategoryProduct", blank=True
    )
    image_alice_id = models.CharField(
        max_length=100, verbose_name="Id картинки в алисе", blank=True, null=True
    )

    def alice_image(self):
        return mark_safe(
            f"<img src='https://avatars.mds.yandex.net/get-dialogs-skill-card/{self.image_alice_id}/one-x2' style='max-width: 300px' />"
        )

    def __str__(self):
        return f"{self.sku}: {self.price} руб."

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"


class Category(models.Model):
    EVENT_CHOICES = [
        ("job", "На работу"),
        ("date", "На свидание"),
        ("every_day", "На каждый день"),
    ]
    CHARACTER_CHOICES = [
        ("romantic_women", "Романтичная и мечтательная"),
        ("buisness_women", "Деловая и решительная"),
        ("sexy_women", "Дерзкая и соблазнительная"),
    ]

    character = models.CharField(
        max_length=50, choices=CHARACTER_CHOICES, verbose_name="Характер"
    )
    event = models.CharField(
        max_length=30, choices=EVENT_CHOICES, verbose_name="Событие"
    )

    products = models.ManyToManyField("Product", through="CategoryProduct", blank=True)

    def __str__(self):
        return f"{self.get_character_display()} | " f"{self.get_event_display()}"

    class Meta:
        ordering = ["event"]
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class CategoryProduct(models.Model):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name="Продукт"
    )
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, verbose_name="Категория"
    )
    position = models.PositiveIntegerField(
        default=0, blank=False, null=False, verbose_name="Позиция"
    )

    def price(self):
        return self.product.price

    class Meta:
        ordering = ["position"]
        verbose_name = "Категория для продукта"
        verbose_name_plural = "Категории для продукта"

    def __str__(self):
        return "Категория или Продукт"
