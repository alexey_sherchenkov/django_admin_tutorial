from django.contrib import admin
from .models import Product, Category
from adminsortable2.admin import SortableInlineAdminMixin


class CategoryInline(SortableInlineAdminMixin, admin.TabularInline):
    model = Product.categories.through
    verbose_name = "Категория"
    verbose_name_plural = "Категории"
    list_fields = ("position", "product", "price")
    readonly_fields = ["price"]
    autocomplete_fields = ["product"]


class ProductInline(admin.TabularInline):
    model = Category.products.through


class ProductAdmin(admin.ModelAdmin):
    exclude = ("categories",)
    # Нужно будет явно перечислить поля модели, которые мы выведем
    # alice_image это метод, который мы определили в модели
    list_fields = [
        "sku",
        "bar_code",
        "description",
        "price",
        "url",
        "image_alice_id",
        "alice_image",
    ]
    readonly_fields = ["alice_image"]
    inlines = [ProductInline]
    list_filter = ["category"]
    search_fields = ["bar_code", "sku"]


class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryInline]
    exclude = ("products",)


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
