# Создание админки для товаров в django

Лучше пройти туторил по джанге. Хотя бы эти.

[Создание приложения, часть про вьюху можно пропустить](https://docs.djangoproject.com/en/3.1/intro/tutorial01/) 

[Модель и базовая админка](https://docs.djangoproject.com/en/3.1/intro/tutorial02/)

[Кастомизация админки](https://docs.djangoproject.com/en/3.1/intro/tutorial07/)

[дока по админки, всю можно не читать, для референса](https://docs.djangoproject.com/en/3.1/ref/contrib/admin/)

[дока по моделям, можно прочитать до части про наследование.](https://docs.djangoproject.com/en/3.1/topics/db/models/)

## Установка и создание приложения

Установите django

```sh
pip install django
```

Создайте проект

```sh
django-admin startproject products_admin
```

Команда создаст такую структуру файлов:

```
products_admin
├── manage.py
└── products_admin
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```

`manage.py` - скрипт для управления проектом. Им можно делать миграции бд, запускать тестовый сервер и т.д.
`products_admin` - папка с основными настройками всего проекта.
`settings.py` - основные настройки проекта. Здесь подключаются приложения, настраивается бд, статика и т.д.
`urls.py` - настройка роутинга для всего проекта
`wsgi.py` - entry-point wsgi сервера

Сейчас можно проверить, что все работает. На http://127.0.0.1:8000/ поднимется сервер.
```sh
python manage.py runserver
```

В django принято разделять компоненты проекта на разные приложения. Сейчас мы сделаем приложение для товаров. Например, если потом в проекте нужны будут промоакции, можно будет сделать отдельное приложение. Но это более актуально, если делать сайт.


```sh
python manage.py startapp products
```
Django создаст папку с приложением.


```
products_admin
├── manage.py
├── products
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   ├── models.py
│   ├── tests.py
│   └── views.py
└── products_admin
```

`admin.py` - создание и кастомизация админки для приложения
`apps.py` - нужен для подключения приложения в настройках проекта
`migrations` - папка, сюда будут складываться миграции бд
`models.py` - ORM модели для бд
`tests.py` - тесты
`views.py` - вьюхи, нам не пригодится

Теперь нужно подключить приложение в настройках проекта. В settings.py в список INSTALLED_APPS добавте


```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'products'  # название нашего приложения
]
```

## Модели

В django есть своя ORM. В ней мы объектами опишем, какие сущности будут хранится в бд.

Начнем созавадть модели в `products/models.py`. Нужно унаследовать класс от django.db.models.Model.

```python
from django.db import models

class Product(models.Model):
    sku = models.CharField(max_length=100)  # CharField поле для хранения небольших строк, max_length обязательный
    bar_code = models.IntegerField()  # Храннение целых чисел
    description = models.TextField(max_length=600) # Хранение длинных строк
    price = models.DecimalField(decimal_places=2, max_digits=8)  # Хранение десятичных чисел
    url = models.URLField()  # хранение url
```

Теперь можно попробовать сделать миграции. Миграции создают или изменяют структуру бд, их нужно делать, если вы добавили поле в модель, переименовали модель, добавили новую и т.д.

```sh
$ python manage.py makemigrations
```

Появился файл `/products/migrations/0001_initial.py`. Там прописано, что django сделает с вашей бд, чтобы провести миграцию. Обычно там все норм и ничего трогать не надо.
Теперь нужно применить эту миграцию, чтобы все изменения записались в бд. В нашем случае Мы создадим таблицу с нашими полями. В какую базу писать, настраивается в settings.py, список DATABASES. По умолчанию используется sqlite3.

```sh
$ python manage.py migrate
```

В консоль выведется, какие миграции применились. Кроме нашей `products` применилось еще куча миграций из стандартной авторизации django.

## Админка

Админка подключена по умолчанию. В файле products_admin/urls.py видим

```python
urlpatterns = [
    path('admin/', admin.site.urls),
]
```

Это значит, что по пути /admin поключин роутинк встроеного приложения админки.

Чтобы попасть в админку, нужно создать 🦸 суперюзера.

```sh
$ python manage.py createsuperuser
```

Вводим имя пользователя, email, пароль. Пароль по умолчанию нужен сложный. Запускаем сервер.

```sh
$ python manage.py runserver
```

Заходим на localhost:8000/admin, логинимся.

![image-20210129114716723](images/image-20210129114716723.png)

Видим только модели юзеров и групп из стандартного приложения авторизации. Теперь нужно добавить в админку нашу модель. Файл `products_admin/products/admin.py`

```python
from django.contrib import admin
from .models import Product

admin.site.register(Product)
```

Появилась наша модель. Если нажать на Products, можно посмотреть все созданные записи для продуктов. Нажмем +Add и заполним поля. Нажмем Save (само ничего не сохраняется, не забывайте). Продукт появится в списке всех продуктов.

![image-20210129115956269.png](images/image-20210129115956269.png)

![image-20210129120300777](images/image-20210129120300777.png)

Какие видим проблемы:
1. В списке нельзя понять, что за товар.
2. Название полей по-английски, заказчик не поймет.
3. Название модели тоже по-английски

Доработаем нашу модель:


```python
class Product(models.Model):
    # verbose_name задает то, как название поля выведется в админке
    sku = models.CharField(max_length=100, verbose_name="СКУ")
    bar_code = models.IntegerField(verbose_name="Штрих код")
    description = models.TextField(max_length=600, verbose_name="Описание аромата")
    price = models.DecimalField(decimal_places=2, max_digits=8, verbose_name="Цена")
    url = models.URLField(verbose_name="Ссылка")

    # В список товаров выводится просто строковое представление объекта модели.
    def __str__(self):
        return f"{self.sku}: {self.price} руб."

    # Вот так исправим название модели в админке
    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
```

Сделаем миграцию

```sh
$ python manage.py makemigrations
$ python manage.py migrate
```
Уже лучше

![](images/image-20210129121159930.png)

![image-20210129121019217](images/image-20210129121019217.png)

## Расширяем модель

Допустим что нам нужно добавлять к товарам категории. Например продукт может подходить людям разных характеров и для разных событий. Там мы сможем в боте выдавать только подходящие товары. 

Добавим модель в `models.py`

```python
class Category(models.Model):
    # В поле типа CharField можно ограничить выбор значений параметром choices
    EVENT_CHOICES = [
        ("job", "На работу"),
        ("date", "На свидание"),
        ("every_day", "На каждый день"),
    ]
    CHARACTER_CHOICES = [
        ("romantic_women", "Романтичная и мечтательная"),
        ("buisness_women", "Деловая и решительная"),
        ("sexy_women", "Дерзкая и соблазнительная"),
    ]

    character = models.CharField(
        max_length=50, choices=CHARACTER_CHOICES, verbose_name="Характер"
    )
    event = models.CharField(
        max_length=30, choices=EVENT_CHOICES, verbose_name="Событие"
    )

    def __str__(self):
        # метод self.get_ПОЛЕ_display() появляется с из магии django. Если
        # вызвать здесь self.event, выведет job, а не На работу
        return (
            f"{self.get_character_display()} | "
            f"{self.get_event_display()}"
        )

    class Meta:
        ordering = ["event"]  # Сортировка в админке по умолчанию
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
```

Зарегисрируем в `products_admin/products/admin.py`

```python
from django.contrib import admin
from .models import Product, Category

admin.site.register(Product)
admin.site.register(Category)
```

При создании категории знаечение можно выбрать в выпадающем списке.

![image-20210129123118132](images/image-20210129123118132.png)


Теперь нужно связать категорию.

В нашем случае, продукт можем быть сразу в нескольких категориях, а в каждой категории сразу несколько товаров. У нас sql бд, для этого нам подойдет связь many to many. Связь делается через промежуточную модель, ее можно будет сделать кастомной. Но пока сделаем простой вариант.

Добавим поля в модели

```python
class Product(models.Model):
    ...  
    categories = models.ManyToManyField("Category", blank=True)  
    # Category это название модели, с которой будет связь. Строкой, потому что
    # она определяется ниже в файле. Но можно передавать и объект.  
    # blank=True позволяет оставлять поле бд пустым. То есть мы можем создать товар без категории

class Category(models.Model):
    ...
    products = models.ManyToManyField("Product", blank=True)  
```

Сделаем миграции и посморим, что получилось.

![image-20210129125149051](images/image-20210129125149051.png)

Можно выделить одну или несколько категорий, нажать save и они добавятся к товару.

## Кастомная промежуточная модель

Допустим, нам нужно хранить порядок, в котором выводятся товары из категории. По желанию заказчика хочется еще просто мерять этот порядок.

В категории товар может быть на разных позициях для вывода, поэтому указывать порядок нужно в той самой промежуточной таблице для many to many. Делаем кастомную промежуточную модель.

```python
class Product(models.Model):
    ...  
    # Добавим параметр through - это и есть промежуточная модель
    categories = models.ManyToManyField("Category", through="CategoryProduct", blank=True)  

class Category(models.Model):
    ...
    products = models.ManyToManyField("Product", through="CategoryProduct", blank=True)  

class CategoryProduct(models.Model):
    # В одной записи в промежуточной таблице у нас должны быть ссылка на наш продукт и на категорию
    # Это связь many to one, делается через ForeignKey
    # on_delete - обязательный параметр. Показывает, что делать, если удалится
    # объект, на который мы ссылаемя
    # CASCADE - если удалится объект, удалитятся все записи про него в промежуточной таблице
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name="Продукт"
    )
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, verbose_name="Категория"
    )
    # null=False - поле не может быть NULL в бд
    # blank=False - поле не может быть пусты в бд
    position = models.PositiveIntegerField(
        default=0, blank=False, null=False, verbose_name="Позиция"
    )

    class Meta:
        ordering = ["position"]
        verbose_name = "Категория для продукта"
        verbose_name_plural = "Категории для продукта"

    def __str__(self):
        return "Категория или Продукт"
```

Если сейчас вы сделаете миграции, то вылетет ошибка `Cannot alter field products.Category.products into products.Category.products - they are not compatible types`. Django не может понять, что там случилось с табличками. Чтобы это исправить, нужно руками править миграции, для туториала мы этого делать не будем. Но если интересно, то [вот так](https://stackoverflow.com/questions/26927705/django-migration-error-you-cannot-alter-to-or-from-m2m-fields-or-add-or-remove).

Мы почистим бд и сделаем миграции с нуля. 

**!!НЕ ДЕЛАЙТЕ ТАК НА ПРОДЕ!!**.

Удалите файл `db.sqlite3` и все файл миграций из папки `products/migration`. Они имееют вид номер_название.py. Не удаляйте саму папку migration и файл `__init__.py`

Сделайте миграции и создайте заново суперюзера.

## Улучшение админки

Из-за кастомной промежуточной таблицы у нас пропал удобный способ добалять товары в категории. Нам нужно добавить Inline, получится вот так

![image-20210129133607144](images/image-20210129133607144.png)

Файл `admin.py`

```python
from django.contrib import admin
from .models import Product, Category

class CategoryInline(admin.TabularInline):
    model = Product.categories.through  # Какая модель встроится inline
    list_fields = ("position", "filter")  # Какие поля нужно показать
    verbose_name = "Категория"
    verbose_name_plural = "Категории"

class ProductInline(admin.TabularInline):
    model = Category.products.through
    list_fields = ("position", "product")

class ProductAdmin(admin.ModelAdmin):
    exclude = ("categories",)  # Исключаем стандартный виджет для добавления категорий
    inlines = [ProductInline]  # Добавляем inline

class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryInline]
    exclude = ("products",)

admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
```

Не очень удобно расставлять по порядку продукты в категориях. Установим плагин 

```sh
$ pip install django-admin-sortable2
```

Подключим его в `settings.py`

```python
INSTALLED_APPS = [
    ...
    "adminsortable2",
]
```

Расширим класс `ProductInline` в `admin.py`

```python
from adminsortable2.admin import SortableAdminMixin

...

# SortableInlineAdminMixin для сортировки использут ordering из класс Meta в промежуточной модели
class CategoryInline(SortableInlineAdminMixin, admin.TabularInline):
    ...
```
Теперь можно перетягивать квадрать в поле sort и сортировать продукты в категории.

![image-20210129135201977](images/image-20210129135201977.png)


## Фильтры и поиск

Когда у нас будет много товаров, будет неплохо их фильтровать. `admin.py`

```python
class ProductAdmin(admin.ModelAdmin):
    ...
    list_filter = ['category']
```

Теперь товары можно фильтровать по категориям.

Сделаем поиск по товаром в инлайне.

```python

class CategoryInline(SortableInlineAdminMixin, admin.TabularInline):
    model = Product.categories.through
    verbose_name = "Категория"
    verbose_name_plural = "Категории"
    list_fields = ("position", "product")
    autocomplete_fields = ["product"]  # автодополнение по search_fields продукта

class ProductAdmin(admin.ModelAdmin):
    exclude = ("categories",)
    inlines = [ProductInline]
    list_filter = ["category"]
    search_fields = ["bar_code", "sku"]  # Поля по которым будем искать определяются в админке продукта
```

## Отображение кастомных данных

Добавим в модель id картинки в алисе и выведем ее в админку.

```python
from django.utils.html import mark_safe
...

class Product(models.Model):
    ...
    image_alice_id = models.CharField(
        max_length=100, verbose_name="Id картинки в алисе", blank=True, null=True
    )

    def alice_image(self):
        #  Так можно вывести голый html в админку
        return mark_safe(
            f"<img src='https://avatars.mds.yandex.net/get-dialogs-skill-card/{self.image_alice_id}/one-x2' style='max-width: 300px' />"
        )

```

```python
class ProductAdmin(admin.ModelAdmin):
    # Нужно будет явно перечислить поля модели, которые мы выведем
    # alice_image это метод, который мы определили в модели
    list_fields = [
        "sku",
        "bar_code",
        "description",
        "price",
        "url",
        "image_alice_id",
        "alice_image",
    ]
    #  alice_image будет только для чтения
    readonly_fields = ["alice_image"]
```

Модель изменилась, делаем миграции

```sh
$ python manage.py makemigrations
$ python manage.py migrate
```

ID для теста `213044/41b3b461e51ce098880e`

![image-20210209161310977](images/image-20210209161310977.png)


Добавим дополнительную ифнормацию в инлайн для категория. Например, выведем цену продукта. В инлайн выводится `Product.categories.through`, то есть модель `CategoryProduct`. В ней нет цены продукта. Нужно определить для этого метод

```python

class CategoryProduct(models.Model):
    ...

    def price(self):
        return self.product.price
```

Добавим в админку

```python
class CategoryInline(SortableInlineAdminMixin, admin.TabularInline):
    ...
    list_fields = ("position", "product", "price")
    readonly_fields = ["price"]
```
